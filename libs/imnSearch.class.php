<?php

	class Result {
			
		private $desctiption;
		private $value;
		private $result;
			
		function __construct($description, $value, $result) {
			$this->description = $description;
			$this->value = $value;
			$this->result = $result;
		}
			
		public function toString() {
			echo 'Result: '.$this->result.'. Method \''.$this->description.'\' worked '.$this->value.'s';
		}
			
	}

	class imnSearch{
	
		private $array;
		private $x;
		private $left;
		private $right;
		
		private $results;
		
		function __construct($x, $array) {
		
			$this->x = $x;
			
			sort($array, SORT_NUMERIC);
			
			$this->array = $array;
			$size = count($this->array);
			if ($size > 0){
				$this->left = 0;
				$this->right = $size-1;
			}
			$this->results = array();
		
		}
		
		private function binarySearchRecursive($start, $end) {
			if ($start > $end) {
				return -1;
			}	
		 
			$mid = ceil(($start + $end) / 2);
			
			if ($this->array[$mid] == $this->x) {
				return $mid;
			} elseif ($this->array[$mid] > $this->x) {
				return $this->binarySearchRecursive($start, $mid - 1);
			} elseif ($this->array[$mid] < $this->x) {
				return $this->binarySearchRecursive($mid + 1, $end);
			}

		}
		
		private function binarySearchItterative() {
			$left = 0;
			$right = count($this->array) - 1;

			while ($left <= $right) {
				$mid = ceil(($left + $right)/2);
				
				if ($this->array[$mid] == $this->x) {
					return $mid;
				} elseif ($this->array[$mid] > $this->x) {
					$right = $mid - 1;
				} elseif ($this->array[$mid] < $this->x) {
					$left = $mid + 1;
				}
			}

			return -1;
		}
		
		private function builtInSearch(){
			if (array_search($this->x, $this->array) != False) {
				return array_search($this->x, $this->array);
			} else {
				return -1;
			}
		}
		
		private function itterationSearch() {
			for ($i = 0; $i < count($this->array); $i++) {
				if ($this->array[$i] == $this->x) {
					return $i;
				}
			}
			return -1;
		}
		
		public function make() {
			$s = microtime();
			$res = $this->binarySearchRecursive($this->left, $this->right);
			if ($res != -1){
			array_push($this->results, new Result('Binary Search - Recursive implementation', 
				(microtime()-$s), $res));
			}
				
			$s = microtime();
			$res = $this->binarySearchItterative();
			if ($res != -1){
			array_push($this->results, new Result('Binary Search - Itterative implementation', 
				(microtime()-$s), $res));
			}
			
			$s = microtime();
			$res = $this->builtInSearch();
			if ($res != -1){
				array_push($this->results, new Result('Binary Search - Built-In implementation', 
					(microtime()-$s), $res));
				
			}
			
			$s = microtime();
			$res = $this->itterationSearch();
			if ($res != -1){
				array_push($this->results, new Result('Itterative searchs', 
					(microtime()-$s), $res));
			}
		}
		
		public function toString(){
			foreach ($this->results as $result) {
				$result->toString();
				echo '<br>';
			}
		}
	
	}