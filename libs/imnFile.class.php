<?php 

class imnFile {
	
	private $size;
	private $min;
	private $max;
	
	private $array;
	
	function __construct($size, $min, $max) {
		$this->size = $size;
		$this->min = $min;
		$this->max = $max;
		
		$this->array = $this->make($size, $min, $max);
	}

	public function make (){
		for ($i = 0; $i < $this->size; $i++){
			if (sizeof($this->array) > 0) {
				
				$rule = True;
				do {
					$newElement = mt_rand($this->min, $this->max);
					for ($j = 0; $j < sizeof($this->array); $j++){
						if ($newElement == $this->array[$j]) {
							break;
						} 
						if ($j == (sizeof($this->array)-1)) {
							$rule = False;
							break;
						}
					}
				} while ($rule == True);
				$this->array[$i] = $newElement;
				
			} else {
				$this->array[$i] = mt_rand($this->min, $this->max);
			}
		}
	}
	
	public function toString() {
		if (is_array($this->array)){
			echo '<pre>';
			print_r($this->array);
			echo '</pre>';
		}
	}

	public function toFile ($file='data.txt') {
		$f = fopen($file, 'a');
		if ($f) {
			for ($i = 0; $i < sizeof($this->array); $i++){
				fwrite($f, $this->array[$i].PHP_EOL);
			} 
			return True;
		} else {
			return False;
		}
	}
}
